import React, { Component } from 'react';
import FullGraph from "./containers/FullGraph";
import SummaryGraph from "./containers/SummaryGraph";
import {DAILY_HIGH, DAILY_LOW, NIGHTLY_HIGH, NIGHTLY_LOW} from "./state";
import {Card, CardText, CardTitle, Toolbar} from "react-md";

class App extends Component {
  render() {
    const separate = (<div className="separate"/>);
    return (
        <React.Fragment>
            <Toolbar
                colored
                title="Room Temperature"/>
            {separate}
            <Card>
                <CardText>
                    <CardTitle title="Hourly Temperatures"/>
                    <FullGraph/>
                </CardText>
            </Card>
            {separate}
            <Card>
                <CardText>
                    <CardTitle title="Nightly Summaries"/>
                    <SummaryGraph highs={NIGHTLY_HIGH} lows={NIGHTLY_LOW}/>
                </CardText>
            </Card>
            {separate}
            <Card>
                <CardText>
                    <CardTitle title="Daily Summaries"/>
                    <SummaryGraph highs={DAILY_HIGH} lows={DAILY_LOW}/>
                </CardText>
            </Card>
            {separate}
        </React.Fragment>
    );
  }
}

export default App;
