// @flow

import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import type {SummaryGraphType} from "../state";

export type VaryingProps = {
    highs: SummaryGraphType,
    lows: SummaryGraphType
}

export type Props = {
    timeZone: ?string,
    highsData: ?Array<Array<number>>,
    lowsData: ?Array<Array<number>>
}

const SummaryGraphComponent = ({timeZone, highsData, lowsData}: Props) => {
    const options = {
        chart: {
            zoomType: 'x'
        },
        title: {
            text: ""
        },
        tooltip: {
            valueDecimals: 2
        },
        xAxis: {
            type: 'datetime'
        },
        time: {
            timezone: timeZone,
            useUTC: false
        },
        series: [
            {
                data: highsData,
                lineWidth: 0.5,
                name: 'High'
            },
            {
                data: lowsData,
                lineWidth: 0.5,
                name: 'Low'

            }]
    };
    if (!highsData || !lowsData) {
        return (<div/>)
    }
    else {
        return (<HighchartsReact highcharts={Highcharts} options={options}/>)
    }
};

export default SummaryGraphComponent