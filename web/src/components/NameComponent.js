// @flow

import React from 'react';

export type Props = {
    name: ?string
}
const NameComponent = ({name}: Props) => {
    return (<div>{name}</div>)
};

export default NameComponent