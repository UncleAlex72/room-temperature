// @flow

import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

export type Props = {
    timeZone: ?string,
    data: ?Array<Array<number>>
}
const FullGraphComponent = ({timeZone, data}: Props) => {
    const options = {
        chart: {
            zoomType: 'x'
        },
        title: {
            text: ''
        },
        tooltip: {
            valueDecimals: 2
        },
        xAxis: {
            type: 'datetime'
        },
        time: {
            timezone: timeZone,
            useUTC: false
        },
        series: [{
            data,
            lineWidth: 0.5,
            name: 'Temperature'
        }]
    };
    if (!data) {
        return (<div/>)
    }
    else {
        return (<HighchartsReact highcharts={Highcharts} options={options}/>)
    }
};

export default FullGraphComponent