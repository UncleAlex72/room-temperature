// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import thunkMiddleware from "redux-thunk"
import App from './App';
import {composeWithDevTools} from "redux-devtools-extension";
import {applyMiddleware, createStore} from "redux";
import {Provider} from "react-redux";
import {initialState} from "./state";
import {reducer} from "./reducer";
import {loadDataAction} from "./actions";
import WebFontLoader from 'webfontloader';
import './index.css';
import 'moment';
import 'moment-timezone';

WebFontLoader.load({
    google: {
        families: ['Roboto:300,400,500,700', 'Material Icons']
    }
});

const store = createStore(
    reducer,
    initialState,
    composeWithDevTools(
        applyMiddleware(
            thunkMiddleware
        )
    )
);

const root = document.getElementById('root');
root && ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    root);

store.dispatch(loadDataAction());