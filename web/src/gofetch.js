// @flow

function gofetch(url: string): Promise<any> {
    return fetch(url, {
        method: 'GET',
        redirect: 'manual',
        credentials : 'same-origin'
    }).then(response => {
        if (response.type === "opaqueredirect") {
            window.location.href = response.url;
            return Promise.resolve(null)
        }
        const status = response.status;
        if (status >= 400) {
            const message = response.statusText;
            return Promise.reject({status, message});
        }
        else {
            return response.json();
        }

    })
}

export default gofetch;