// @flow

import { connect } from 'react-redux'
import NameComponent from '../components/NameComponent'
import {getName} from "../reducer";
import type {Props} from "../components/NameComponent";
import type {State} from "../state";

const mapStateToProps: State => Props = state => {
    const name = getName(state);
    return {name};
};

const Name = connect(
    mapStateToProps,
)(NameComponent);

export default Name