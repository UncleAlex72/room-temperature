// @flow

import { connect } from 'react-redux'
import {getSummaryGraph, getTimeZone} from "../reducer";
import type {State} from "../state";
import SummaryGraphComponent from "../components/SummaryGraphComponent";
import type {Props, VaryingProps} from "../components/SummaryGraphComponent";

const mapStateToProps: (State, VaryingProps) => Props = (state, varyingProps) => {
    const highsData = getSummaryGraph(state, varyingProps.highs);
    const lowsData = getSummaryGraph(state, varyingProps.lows);
    const timeZone = getTimeZone(state);
    return {timeZone, highsData, lowsData};
};

const SummaryGraph = connect(
    mapStateToProps,
)(SummaryGraphComponent);

export default SummaryGraph