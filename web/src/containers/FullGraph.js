// @flow

import { connect } from 'react-redux'
import FullGraphComponent from '../components/FullGraphComponent'
import {getFullGraph, getTimeZone} from "../reducer";
import type {State} from "../state";
import type {Props} from "../components/FullGraphComponent";

const mapStateToProps: State => Props = state => {
    const timeZone = getTimeZone(state);
    const data = getFullGraph(state);
    return {timeZone, data};
};

const FullGraph = connect(
    mapStateToProps,
)(FullGraphComponent);

export default FullGraph