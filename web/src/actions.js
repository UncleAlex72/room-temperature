// @flow

import type {Graphs, HTTPError} from "./state";
import gofetch from "./gofetch";

export const USER_LOADED = "USER_LOADED";
export const GRAPHS_LOADED = "GRAPHS_LOADED";
export const SHOW_ERROR = "SHOW_ERROR";
export const HIDE_ERROR = "HIDE_ERROR";
export const INITIALISED = "INITIALISED";

export type UserLoadedAction = {
    type: "USER_LOADED",
    name: string
}

export type GraphsLoadedAction = {
    type: "GRAPHS_LOADED",
    graphs: Graphs
}

export type InitialisedAction = {
    type: "INITIALISED"
}

export type ShowErrorAction = {
    type: "SHOW_ERROR",
    error: HTTPError,
}

export type HideErrorAction = {
    type: "HIDE_ERROR"
}

export type Action = UserLoadedAction | GraphsLoadedAction | InitialisedAction | ShowErrorAction | HideErrorAction

// eslint-disable-next-line
type ThunkAction = (dispatch: Dispatch) => any;
type PromiseAction = Promise<Action>;
type Dispatch = (action: Action | ThunkAction | PromiseAction) => any;

export function userLoadedAction(name: string): UserLoadedAction {
    return {
        type: USER_LOADED,
        name
    };
}

export function graphsLoadedAction(graphs: Graphs): GraphsLoadedAction {
    return {
        type: GRAPHS_LOADED,
        graphs
    };
}

export function showError(error: HTTPError): ShowErrorAction {
    return {
        type: SHOW_ERROR,
        error,
    }
}

export function loadDataAction(): ThunkAction {
    return function(dispatch) {
        gofetch("/room-temperature/userinfo").then(json => {
            dispatch(userLoadedAction(json.firstName))
        }).then(result => {
            gofetch("/room-temperature/graphs").then(graphs => {
                dispatch(graphsLoadedAction(graphs))
            })
        }).catch(reason => {
            dispatch(showError(reason));
        })

    };
}
