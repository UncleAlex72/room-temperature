// @flow

import type {Graph, Graphs, State, SummaryGraphType} from "./state";
import {DAILY_HIGH, DAILY_LOW, NIGHTLY_HIGH, NIGHTLY_LOW} from "./state";
import type {Action, GraphsLoadedAction, ShowErrorAction, UserLoadedAction} from "./actions";
import {GRAPHS_LOADED, HIDE_ERROR, INITIALISED, SHOW_ERROR, USER_LOADED} from "./actions";
import {createSelectorWithDependencies} from "reselect-tools";

export function reducer(state: State, action: Action) {
    switch (action.type) {
        case INITIALISED:
            return {
                ...state,
                initialised: true
            };
        case SHOW_ERROR:
            const error = (action: ShowErrorAction).error;
            return {
                ...state,
                error
            };
        case HIDE_ERROR:
            return {
                ...state,
                error: null
            };
        case GRAPHS_LOADED:
            const graphs = (action: GraphsLoadedAction).graphs;
            return {
                ...state,
                graphs
            };
        case USER_LOADED:
            const name = (action: UserLoadedAction).name;
            return {
                ...state,
                name
            };
        default: {
            return state;
        }
    }
}

export const getName: State => ?string = state => state.name;

export const getGraphs: State => ?Graphs = state => state.graphs;

export const getTimeZone: State => ?string = createSelectorWithDependencies(
    [getGraphs],
    (graphs: ?Graphs) => graphs && graphs.zoneId
);

const getGraph: (Graphs => Graph) => State => ?Graph = fn => createSelectorWithDependencies(
    [getGraphs],
    (graphs: ?Graphs) => graphs && fn(graphs)
);

export const getFullGraph =
    getGraph(graphs => graphs.full);

const summaryGraphTypeBuilder: SummaryGraphType => Graphs => Graph = summaryGraphType => {
    switch (summaryGraphType) {
        case NIGHTLY_HIGH:
            return graphs => graphs.nightlyHighs;
        case NIGHTLY_LOW:
            return graphs => graphs.nightlyLows;
        case DAILY_HIGH:
            return graphs => graphs.dailyHighs;
        case DAILY_LOW:
            return graphs => graphs.dailyLows;
        default:
            throw new Error(`${summaryGraphType} is not a valid summary graph type.`);
    }
};

export function getSummaryGraph(state: State, summaryGraphType: SummaryGraphType): ?Graph {
    return getGraph(summaryGraphTypeBuilder(summaryGraphType))(state);
}
