// @flow

export type Graph = Array<Array<number>>

export type SummaryGraphType = "NIGHTLY_HIGH" | "NIGHTLY_LOW" | "DAILY_HIGH" | "DAILY_LOW"

export const NIGHTLY_HIGH = "NIGHTLY_HIGH";
export const NIGHTLY_LOW = "NIGHTLY_LOW";
export const DAILY_HIGH = "DAILY_HIGH";
export const DAILY_LOW = "DAILY_LOW";

export type Graphs = {
    zoneId: string,
    full: Graph,
    nightlyHighs: Graph,
    nightlyLows: Graph,
    dailyHighs: Graph,
    dailyLows: Graph
}

export type HTTPError = {
    status: number,
    message: string
}

export type State = {
    error?: HTTPError,
    initialising: boolean,
    name?: string,
    graphs? : Graphs
}

export const initialState = {
    showErrorDialogue: false,
    initialising: true
};
