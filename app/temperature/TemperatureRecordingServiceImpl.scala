package temperature

import java.time.format.DateTimeFormatter
import java.time.{Clock, ZonedDateTime}

import com.typesafe.scalalogging.StrictLogging
import time.TimeTruncator

import scala.concurrent.{ExecutionContext, Future}

class TemperatureRecordingServiceImpl(val thermometer: Thermometer,
                                      val temperatureDao: TemperatureDao,
                                      val clock: Clock,
                                      val timeTruncator: TimeTruncator)
                                     (implicit ec: ExecutionContext) extends TemperatureRecordingService with StrictLogging {

  override def storeCurrentTemperature(): Future[TemperatureReading] = {
    for {
      currentTemperature <- {
        logger.info("Checking for the current temperature.")
        thermometer.currentTemperature()
      }
      persistedCurrentTemperature <- {
        val now: ZonedDateTime = clock.instant().atZone(clock.getZone)
        val nowish: ZonedDateTime = timeTruncator.truncate(now)
        logger.info(
          s"Storing a new temperature of $currentTemperature at ${DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(nowish)}")
        temperatureDao.insert(currentTemperature, nowish.toInstant)
      }
    } yield {
      persistedCurrentTemperature
    }
  }
}
