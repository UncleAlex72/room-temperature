package temperature

import akka.stream.Materializer
import akka.stream.scaladsl.Source

import scala.concurrent.{ExecutionContext, Future}

abstract class AkkaSourceThermometer(implicit val materializer: Materializer, val ec: ExecutionContext) extends Thermometer {

  def buildSource(): Source[BigDecimal, _]

  private val zero = BigDecimal(0)

  override def currentTemperature(): Future[BigDecimal] = {
    buildSource().runFold(zero)((_, value) => value)
  }
}
