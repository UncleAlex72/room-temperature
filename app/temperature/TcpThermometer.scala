package temperature

import akka.actor.ActorSystem
import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Framing, Source, Tcp}
import akka.util.ByteString

import scala.concurrent.{ExecutionContext, Future}

class TcpThermometer(val host: String, port: Int)
                    (implicit val actorSystem: ActorSystem, override val materializer: Materializer, override val ec: ExecutionContext) extends AkkaSourceThermometer {

  override def buildSource(): Source[BigDecimal, _] = {
    val flow: Flow[ByteString, ByteString, Future[Tcp.OutgoingConnection]] = Tcp().outgoingConnection(host, port)
      .via(Framing.delimiter(delimiter = ByteString('\n'), maximumFrameLength = Int.MaxValue, allowTruncation = true))
    Source.maybe[ByteString].via(flow).map { by =>
      val str: String = by.utf8String.trim
      BigDecimal(str)
    }
  }
}