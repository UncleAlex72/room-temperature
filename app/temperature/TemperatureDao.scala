package temperature

import java.time.Instant

import scala.concurrent.Future

trait TemperatureDao {

  def insert(temperature: BigDecimal, when: Instant): Future[TemperatureReading]

  def findAll(): Future[Seq[TemperatureReading]]

  def isEmpty(): Future[Boolean]

  def findDuring(maybeEarliestInstant: Option[Instant], maybeLatestInstant: Option[Instant]): Future[Seq[TemperatureReading]]
}
