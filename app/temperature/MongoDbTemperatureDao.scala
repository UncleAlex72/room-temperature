package temperature

import java.time.{Clock, Instant}

import com.typesafe.scalalogging.StrictLogging
import reactivemongo.api.DefaultDB
import reactivemongo.api.indexes.{Index, IndexType}
import uk.co.unclealex.mongodb.Bson._
import uk.co.unclealex.mongodb.{MongoDbDao, Query, Sort}

import scala.concurrent.{ExecutionContext, Future}

class MongoDbTemperatureDao(override val databaseProvider: () => Future[DefaultDB], override val clock: Clock)
                           (implicit executionContext: ExecutionContext)
  extends MongoDbDao[TemperatureReading](databaseProvider, clock, "temperature", MongoDbTemperatureDao.whenIndex)
  with TemperatureDao with StrictLogging {

  override def insert(temperature: BigDecimal, when: Instant): Future[TemperatureReading] = {
    upsert("when" === when, tr => tr.copy(temperature = temperature, when = when))
  }

  override def findDuring(maybeEarliestInstant: Option[Instant], maybeLatestInstant: Option[Instant]): Future[Seq[TemperatureReading]] = {
    findWhere("when" ?>=&<=(maybeEarliestInstant, maybeLatestInstant))
  }

  override def isEmpty(): Future[Boolean] = findOne(Query.all).map(_.isEmpty)

  override def defaultSort(): Option[Sort] = {
    "when".asc
  }

  override def findAll(): Future[Seq[TemperatureReading]] = {
    super[MongoDbDao].findAll()
  }
}

object MongoDbTemperatureDao {

  val whenIndex: Index = Index(key = Seq("when" -> IndexType.Ascending), name = Some("when_index"))

}