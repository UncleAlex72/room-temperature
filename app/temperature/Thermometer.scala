package temperature

import scala.concurrent.Future

trait Thermometer {

  def currentTemperature(): Future[BigDecimal]
}
