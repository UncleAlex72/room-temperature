package temperature

import java.time.Instant

import play.api.libs.json.{Format, Json}
import reactivemongo.bson.{BSONDocumentHandler, Macros}
import uk.co.unclealex.mongodb.{ID, IsPersistable, IsUpsertable}

case class TemperatureReading(_id: Option[ID] = None, temperature: BigDecimal, when: Instant)

object TemperatureReading {

  import uk.co.unclealex.mongodb.Bson._

  implicit val temperatureReadingIsPersistable: IsPersistable[TemperatureReading] = IsPersistable(
    getId = _._id,
    setId = id => _.copy(_id = Some(id))
  )

  implicit val temperatureReadingHandler: BSONDocumentHandler[TemperatureReading] = Macros.handler[TemperatureReading]
  implicit val temperatureReadingFormat: Format[TemperatureReading] = Json.format[TemperatureReading]

  implicit val temperatureReadingIsUpsertable: IsUpsertable[TemperatureReading] = IsUpsertable(
    TemperatureReading(None, BigDecimal(0), Instant.EPOCH))
}