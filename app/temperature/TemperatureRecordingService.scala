package temperature

import scala.concurrent.Future

trait TemperatureRecordingService {

  def storeCurrentTemperature(): Future[TemperatureReading]
}
