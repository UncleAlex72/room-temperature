package temperature

import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor
import java.time.{Instant, OffsetDateTime, ZonedDateTime}

import com.typesafe.scalalogging.StrictLogging
import time.TimeTruncator

import scala.collection.SortedMap
import scala.util.{Failure, Success, Try}

class LegacyLogReaderServiceImpl(timeTruncator: TimeTruncator) extends LegacyLogReaderService with StrictLogging {

  // Read dates and times such as Thu 26 Jul 18:11:01 UTC 2018
  val formatter: DateTimeFormatter =
    DateTimeFormatter.ofPattern("EEE d MMM HH:mm:ss z yyyy")

  override def read(lines: Seq[String]): SortedMap[Instant, BigDecimal] = {
    case class State(
                      temperaturesByTime: SortedMap[Instant, BigDecimal] = SortedMap.empty,
                      maybeParsedDate: Option[Instant] = None)

    val state: State = lines.foldLeft(State()) { (previousState, originalLine) =>
      val line = originalLine.replaceAll("""\s+""", " ").trim
      val dateParsing: Try[ZonedDateTime] = Try(formatter.parse(line, (temporalAccessor: TemporalAccessor) => {
        ZonedDateTime.from(temporalAccessor)
      }))
      dateParsing match {
        case Success(zonedDateTime) =>
          val truncatedOffsetDateTime: OffsetDateTime = timeTruncator.truncate(zonedDateTime).toOffsetDateTime
          previousState.copy(maybeParsedDate = Some(truncatedOffsetDateTime.toInstant))
        case Failure(_) =>
          logger.info(s"Could not read line '$line' as a date.")
          val maybeTemperature: Option[BigDecimal] = Try(BigDecimal(line)).toOption
          val maybeNewState: Option[State] = for {
            parsedDate <- previousState.maybeParsedDate
            temperature <- maybeTemperature
          } yield {
            logger.info(s"Found temperature $temperature at $parsedDate")
            val temperaturesByTime: SortedMap[Instant, BigDecimal] = previousState.temperaturesByTime
            val newTemperaturesByTime: SortedMap[Instant, BigDecimal] = temperaturesByTime.get(parsedDate) match {
              case Some(currentTemperature) if currentTemperature >= temperature  => temperaturesByTime
              case _ =>
                logger.info(s"Adding temperate $temperature at $parsedDate")
                temperaturesByTime + (parsedDate -> temperature)
            }
            previousState.copy(temperaturesByTime = newTemperaturesByTime)
          }
          maybeNewState.getOrElse(previousState)
      }
    }
    state.temperaturesByTime
  }
}