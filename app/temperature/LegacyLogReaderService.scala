package temperature

import java.time.{Instant, OffsetDateTime}

import scala.collection.SortedMap

trait LegacyLogReaderService {

  def read(lines: Seq[String]): SortedMap[Instant, BigDecimal]
}
