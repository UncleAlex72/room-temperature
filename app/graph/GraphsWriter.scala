package graph

import play.api.libs.json.Writes

trait GraphsWriter extends Writes[GraphsContainer]