package graph

import java.time.{Instant, ZoneId}

import play.api.libs.json.{JsArray, Json, Writes}
import reactivemongo.bson.{BSONDocumentHandler, BSONHandler, BSONString, Macros}
import uk.co.unclealex.mongodb._
import uk.co.unclealex.mongodb.Bson._


case class GraphsContainer(_id: Option[ID],
                           zoneId: ZoneId,
                           full: Seq[Point],
                           dailyLows: Seq[Point],
                           dailyHighs: Seq[Point],
                           nightlyLows: Seq[Point],
                           nightlyHighs: Seq[Point],
                           lastUpdated: Option[Instant])
object GraphsContainer {

  private implicit def zoneIdHandler(implicit stringHandler: BSONHandler[BSONString, String]): BSONHandler[BSONString, ZoneId] = {
    stringHandler.as[ZoneId](ZoneId.of, _.getId)
  }

  implicit val graphsContainerIsPersistable: IsPersistable[GraphsContainer] = IsPersistable.idAndLastUpdated(
    getId = _._id,
    setId = id => _.copy(_id = Some(id)),
    getLastUpdated = _.lastUpdated,
    setLastUpdated = instant => _.copy(lastUpdated = Some(instant)),
  )

  implicit def graphsContainerIsUpsertable(implicit zoneId: ZoneId): IsUpsertable[GraphsContainer] = IsUpsertable(
    GraphsContainer(
      _id = None,
      zoneId = zoneId,
      full = Seq.empty,
      dailyLows = Seq.empty,
      dailyHighs = Seq.empty,
      nightlyHighs = Seq.empty,
      nightlyLows = Seq.empty,
      lastUpdated = None))

  implicit val graphsContainerHandler: BSONDocumentHandler[GraphsContainer] = Macros.handler[GraphsContainer]
  implicit val graphsContainerWrites: Writes[GraphsContainer] = Json.writes[GraphsContainer]

}

case class Point(x: Instant, y: BigDecimal)

object Point {

  implicit val pointHandler: BSONDocumentHandler[Point] = Macros.handler[Point]

  implicit def pointWrites(implicit longWrites: Writes[Long],
                           bigDecimalWrites: Writes[BigDecimal]): Writes[Point] = (point: Point) => {
    JsArray(Seq(longWrites.writes(point.x.toEpochMilli), bigDecimalWrites.writes(point.y)))
  }
}