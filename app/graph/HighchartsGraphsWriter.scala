package graph
import java.time.{Clock, LocalDate, ZonedDateTime}

import play.api.libs.json._

import scala.collection.SortedMap

class HighchartsGraphsWriter(val clock: Clock) extends GraphsWriter {


  def writer[K](keyFactory: K => ZonedDateTime): Writes[SortedMap[K, BigDecimal]] = (map: SortedMap[K, BigDecimal]) => {
    JsArray(map.toSeq.map {
      case (k, v) => JsArray(Seq(Json.toJson(keyFactory(k).toInstant.toEpochMilli), Json.toJson(v)))
    })
  }

  implicit val zonedDateTimeWriter: Writes[SortedMap[ZonedDateTime, BigDecimal]] = writer(identity)

  implicit val localDateTimeWriter: Writes[SortedMap[LocalDate, BigDecimal]] = writer { ld =>
    ld.atStartOfDay().atZone(clock.getZone)
  }

  implicit val graphsContainerFormat: Writes[GraphsContainer] = Json.writes[GraphsContainer]

  override def writes(o: GraphsContainer): JsValue = graphsContainerFormat.writes(o)
}
