package graph
import java.time.Instant

trait GraphsGenerator {

  def generateGraphs(readings: Seq[(Instant, BigDecimal)]): GraphsContainer
}
