package graph

import java.time._
import java.time.temporal.{ChronoUnit, TemporalUnit}

import ordering.JavaTimeOrderings._

import scala.collection.SortedMap

class GraphsGeneratorImpl(val clock: Clock) extends GraphsGenerator {

  val timeZone: ZoneId = clock.getZone

  def toDayGenerator(instant: Instant): Option[Instant] = {
    val zonedDateTime: ZonedDateTime = instant.atZone(timeZone)
    val hour: Int = zonedDateTime.getHour
    if (hour >= 9 && hour < 21) {
      Some(zonedDateTime.truncatedTo(ChronoUnit.DAYS).withHour(12).toInstant)
    }
    else {
      None
    }
  }

  def toNightGenerator(instant: Instant): Option[Instant] = {
    val zonedDateTime: ZonedDateTime = instant.atZone(timeZone)
    val hour: Int = zonedDateTime.getHour
    if (hour < 9) {
      Some(zonedDateTime.truncatedTo(ChronoUnit.DAYS).toInstant)
    }
    else if (hour >= 21) {
      Some(zonedDateTime.truncatedTo(ChronoUnit.DAYS).plusDays(1).toInstant)
    }
    else {
      None
    }
  }

  def generateGraphs(readings: Seq[(Instant, BigDecimal)]): GraphsContainer = {

    val buildFullGraph: GraphBuilder = buildGraph(Option(_), (x, _) => x)
    val buildDailyHighs: GraphBuilder = buildGraph(toDayGenerator, _.max(_))
    val buildDailyLows: GraphBuilder = buildGraph(toDayGenerator, _.min(_))
    val buildNightlyHighs: GraphBuilder = buildGraph(toNightGenerator, _.max(_))
    val buildNightlyLows: GraphBuilder = buildGraph(toNightGenerator, _.min(_))
    case class Graphs(
                       full: Graph = SortedMap.empty,
                       dailyHighs: Graph = SortedMap.empty,
                       dailyLows: Graph = SortedMap.empty,
                       nightlyHighs: Graph = SortedMap.empty,
                       nightlyLows: Graph = SortedMap.empty)
    val graphs: Graphs = readings.foldLeft(Graphs()){ (graphs, reading) =>
      graphs.copy(
        full = buildFullGraph(graphs.full, reading),
        dailyHighs = buildDailyHighs(graphs.dailyHighs, reading),
        dailyLows = buildDailyLows(graphs.dailyLows, reading),
        nightlyHighs = buildNightlyHighs(graphs.nightlyHighs, reading),
        nightlyLows = buildNightlyLows(graphs.nightlyLows, reading),
      )
    }
    def toPoints(graph: Graph): Seq[Point] = graph.toSeq.map { case (x, y) => Point(x, y) }
    GraphsContainer(
      _id = None,
      zoneId = timeZone,
      full = toPoints(graphs.full),
      dailyLows = toPoints(graphs.dailyLows),
      dailyHighs = toPoints(graphs.dailyHighs),
      nightlyLows = toPoints(graphs.nightlyLows),
      nightlyHighs = toPoints(graphs.nightlyHighs),
      lastUpdated = None)
  }

  def buildGraph(keyGenerator: Instant => Option[Instant],
                 aggregator: (BigDecimal, BigDecimal) => BigDecimal)
                (graph: SortedMap[Instant, BigDecimal], reading: (Instant, BigDecimal)): SortedMap[Instant, BigDecimal] = {
    val (when, temperature) = reading
    keyGenerator(when).foldLeft(graph){ (previousGraph, key) =>
      val newValue: BigDecimal = previousGraph.get(key).foldLeft(temperature)(aggregator)
      previousGraph + (key -> newValue)
    }
  }

  type Graph = SortedMap[Instant, BigDecimal]
  type GraphBuilder = (Graph, (Instant, BigDecimal)) => Graph
}
