package graph

import java.time.{Clock, ZoneId}

import reactivemongo.api.DefaultDB
import uk.co.unclealex.mongodb.Bson._
import uk.co.unclealex.mongodb.{MongoDbDao, Query}

import scala.concurrent.{ExecutionContext, Future}

class MongoDbGraphsContainerDao(override val databaseProvider: () => Future[DefaultDB], clock: Clock)(implicit ec: ExecutionContext)
  extends MongoDbDao[GraphsContainer](databaseProvider, clock, "graphs")
    with ReadOnlyGraphsContainerDao with WriteOnlyGraphsContainerDao {

  implicit val zoneId: ZoneId = clock.getZone

  override def getGraphsContainer: Future[Option[GraphsContainer]] = {
    findOne(Query.all)
  }

  override def update(graphsContainer: GraphsContainer): Future[Unit] = {
    Future.successful {
      val builder: GraphsContainer => GraphsContainer = existingGraphsContainer =>
        existingGraphsContainer.copy(
          full = graphsContainer.full,
          dailyHighs = graphsContainer.dailyHighs,
          dailyLows = graphsContainer.dailyLows,
          nightlyHighs = graphsContainer.nightlyHighs,
          nightlyLows = graphsContainer.nightlyLows
        )
      upsert(Query.all, builder)
    }
  }
}
