package graph

import com.typesafe.scalalogging.StrictLogging
import play.api.libs.json.Json
import temperature.TemperatureDao

import scala.concurrent.{ExecutionContext, Future}

class GraphsServiceImpl(
                         val temperatureDao: TemperatureDao,
                         val graphsGenerator: GraphsGenerator,
                         val writeOnlyGraphsContainerDao: WriteOnlyGraphsContainerDao)
                       (implicit val executionContext: ExecutionContext, graphWriter: GraphsWriter)
  extends GraphsService with StrictLogging {

  override def generateAndStore(): Future[GraphsContainer] = {
    logger.info("Generating graphs.")
    for {
      readings <- temperatureDao.findAll()
      temperatures = readings.map(r => r.when -> r.temperature)
      graphsContainer = graphsGenerator.generateGraphs(temperatures)
      _ <- writeOnlyGraphsContainerDao.update(graphsContainer)
    }  yield {
      logger.info("FullGraph generation completed.")
      graphsContainer
    }
  }
}
