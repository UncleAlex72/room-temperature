package graph

import scala.concurrent.Future

trait WriteOnlyGraphsContainerDao {

  def update(graphsContainer: GraphsContainer): Future[Unit]
}
