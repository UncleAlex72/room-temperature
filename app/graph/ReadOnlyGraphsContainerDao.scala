package graph

import scala.concurrent.Future

trait ReadOnlyGraphsContainerDao {

  def getGraphsContainer: Future[Option[GraphsContainer]]
}
