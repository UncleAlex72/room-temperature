package graph

import scala.concurrent.Future

trait GraphsService {

  def generateAndStore(): Future[GraphsContainer]
}
