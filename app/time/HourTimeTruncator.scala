package time
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

class HourTimeTruncator extends TimeTruncator {

  override def truncate(zonedDateTime: ZonedDateTime): ZonedDateTime = {
    zonedDateTime.truncatedTo(ChronoUnit.HOURS)
  }
}
