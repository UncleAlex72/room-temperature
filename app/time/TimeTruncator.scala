package time

import java.time.ZonedDateTime

trait TimeTruncator {

  def truncate(zonedDateTime: ZonedDateTime): ZonedDateTime
}
