package ordering

import java.time.{LocalDate, ZonedDateTime}

object JavaTimeOrderings {

  implicit val zonedDateTimeOrdering: Ordering[ZonedDateTime] = Ordering.by(_.toInstant)

  implicit val localDateOrdering: Ordering[LocalDate] = (x: LocalDate, y: LocalDate) => x.compareTo(y)
}
