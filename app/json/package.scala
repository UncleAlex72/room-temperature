import java.time.format.DateTimeFormatter
import java.time.{Instant, OffsetDateTime, ZoneId, ZonedDateTime}

import play.api.libs.json._

import scala.util.{Failure, Success, Try}

/**
  * Json codecs for various types
  */
package object json {

  implicit def instantFormat(implicit longFormat: Format[Long]): Format[Instant] = new Format[Instant] {
    override def writes(o: Instant): JsValue = longFormat.writes(o.toEpochMilli)

    override def reads(json: JsValue): JsResult[Instant] = longFormat.reads(json).map(ms => Instant.ofEpochMilli(ms))
  }

  implicit def zonedDateTimeFormat(implicit stringFormat: Format[String], zoneId: ZoneId): Format[ZonedDateTime] = new Format[ZonedDateTime] {
    val formatter: DateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

    override def reads(json: JsValue): JsResult[ZonedDateTime] = stringFormat.reads(json).flatMap { str =>
      Try(OffsetDateTime.parse(str, formatter)) match {
        case Success(dateTime) => JsSuccess(dateTime.atZoneSameInstant(zoneId))
        case Failure(_) => JsError(JsonValidationError(s"Cannot parse '$str' as a date and time."))
      }
    }

    override def writes(zdt: ZonedDateTime): JsValue = {
      stringFormat.writes(zdt.toOffsetDateTime.format(formatter))
    }
  }
}
