package scheduler

import java.time.Clock

import akka.actor.{Actor, Props}
import akka.event.Logging
import graph.GraphsService
import scheduler.RetrieveAndStoreTemperatureActor.Poll
import temperature.TemperatureRecordingService

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

class RetrieveAndStoreTemperatureActor(
                                        val temperatureRecordingService: TemperatureRecordingService,
                                        val graphsService: GraphsService,
                                        val clock: Clock) extends Actor {

  val log = Logging(context.system, this)

  implicit val ec: ExecutionContext = context.dispatcher

  def receive: PartialFunction[Any, Unit] = {
    case Poll =>
      log.info("Sending a request for the current temperature.")
      temperatureRecordingService.storeCurrentTemperature().andThen {
        case Success(reading) =>
          log.info(s"Stored temperature ${reading.temperature}")
          graphsService.generateAndStore()
        case Failure(t) =>
          log.error(t, "Could not get the current temperature.")
      }
  }
}

object RetrieveAndStoreTemperatureActor {

  object Poll

  def props(temperatureRecordingService: TemperatureRecordingService, graphsService: GraphsService, clock: Clock): Props =
    Props(new RetrieveAndStoreTemperatureActor(temperatureRecordingService, graphsService, clock))

}