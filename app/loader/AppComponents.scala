package loader

import java.time.format.DateTimeFormatter
import java.time.{Clock, OffsetDateTime}
import java.util.Date

import akka.actor.{ActorRef, ActorSystem}
import akka.stream.Materializer
import auth._
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import com.typesafe.scalalogging.StrictLogging
import controllers._
import graph._
import loader.ConfigLoaders._
import play.api.ApplicationLoader
import play.api.cache.ehcache.EhCacheComponents
import play.api.libs.ws.ahc.AhcWSComponents
import play.api.mvc._
import play.api.routing.Router
import router.Routes
import routers.SpaRouter
import scheduler.RetrieveAndStoreTemperatureActor
import scheduler.RetrieveAndStoreTemperatureActor.Poll
import temperature._
import time.{HourTimeTruncator, TimeTruncator}
import uk.co.unclealex.psm.{PlaySilhouetteMongoFromContext, SilhouetteAuthorization}

import scala.concurrent.Future
import scala.concurrent.duration._

/**
  * Create all the components needed to run this application.
  * @param context The Play Context.
  */
class AppComponents(context: ApplicationLoader.Context)
  extends PlaySilhouetteMongoFromContext(context, "room-temperature")
    with EhCacheComponents
    with AhcWSComponents
    with AssetsComponents
    with StrictLogging {

  def redirectOnLogin: Call = Call("GET", "/room-temperature/")
  def redirectOnLogout: Call = Call("GET", "/room-temperature/")

  val clock: Clock = Clock.systemDefaultZone()


  val validUsers: Seq[String] = configuration.get[String]("silhouette.emails").split(',').map(_.trim)
  val authorization: SilhouetteAuthorization = new ValidUserAuthorization(validUsers)

  val timeTruncator: TimeTruncator = new HourTimeTruncator()

  val temperatureDao = new MongoDbTemperatureDao(
    databaseProvider = databaseProvider,
    clock = clock)

  val graphsGenerator = new GraphsGeneratorImpl(
    clock = clock)

  val graphsContainerDao = new MongoDbGraphsContainerDao(databaseProvider, clock)

  implicit val graphsWriter: GraphsWriter = new HighchartsGraphsWriter(clock)

  val graphsService = new GraphsServiceImpl(
    temperatureDao = temperatureDao,
    graphsGenerator = graphsGenerator,
    writeOnlyGraphsContainerDao = graphsContainerDao
  )

  val legacyLogReaderService = new LegacyLogReaderServiceImpl(
    timeTruncator = timeTruncator
  )

  implicit val _actorSystem: ActorSystem = actorSystem
  implicit val _materializer: Materializer = materializer

  val maybeThermometerConfiguration: Option[ThermometerConfiguration] = configuration.get[Option[ThermometerConfiguration]]("thermometer")
  val maybeThermometer: Option[Thermometer] = maybeThermometerConfiguration.map { thermometerConfiguration =>
    new TcpThermometer(thermometerConfiguration.host, thermometerConfiguration.port)
  }

  maybeThermometer.foreach { thermometer =>
    val healthCheck: () => Future[String] = () => thermometer.currentTemperature().map(_ => "ok")
    registerHealthCheck("thermometer", healthCheck, 1.minute)
  }
  val maybeTemperatureRecordingService: Option[TemperatureRecordingService] = maybeThermometer.map { thermometer =>
    val temperatureRecordingService = new TemperatureRecordingServiceImpl(
      thermometer  = thermometer,
      temperatureDao = temperatureDao,
      timeTruncator = timeTruncator,
      clock = clock
    )

    val retrieveAndStoreTemperatureActor: ActorRef =
      actorSystem.actorOf(
        RetrieveAndStoreTemperatureActor.props(
          temperatureRecordingService = temperatureRecordingService,
          graphsService = graphsService,
          clock = clock),
        "retrieveAndStoreTemperatureActor")
    val scheduler: QuartzSchedulerExtension = QuartzSchedulerExtension(actorSystem)
    val nextRunDate: Date = scheduler.schedule("EveryHour", retrieveAndStoreTemperatureActor, Poll, None)
    logger.info {
      val nextOffsetDateTime: OffsetDateTime = nextRunDate.toInstant.atZone(clock.getZone).toOffsetDateTime
      s"Temperature gathering will begin at ${DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(nextOffsetDateTime)}"
    }
    temperatureRecordingService
  }

  val importController = new ImportController(
    silhouette = silhouette,
    authorization = authorization,
    legacyLogReaderService = legacyLogReaderService,
    temperatureDao = temperatureDao,
    controllerComponents = controllerComponents)

  val graphsController = new GraphsController(
    silhouette = silhouette,
    authorization = authorization,
    clock = clock,
    readOnlyGraphsContainerDao = graphsContainerDao,
    controllerComponents = controllerComponents
  )
  graphsService.generateAndStore()

  val userInfoController = new UserInfoController(
    silhouette = silhouette,
    authorization = authorization,
    controllerComponents = controllerComponents)

  val temperatureController = new TemperatureController(
    maybeTemperatureRecordingService = maybeTemperatureRecordingService,
    controllerComponents = controllerComponents)

  override def router: Router = new Routes(
    httpErrorHandler,
    importController,
    temperatureController,
    socialAuthController,
    graphsController,
    userInfoController,
    secureMetricsController,
    healthCheckController,
    new SpaRouter(assets)
  )

}
