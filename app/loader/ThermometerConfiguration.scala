package loader

/**
  * A model used to configure the location of the thermometer.
  */
case class ThermometerConfiguration(host: String, port: Int)