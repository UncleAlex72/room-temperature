package controllers

import play.api.libs.json.{Format, Json}
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import uk.co.unclealex.psm._

class UserInfoController(
                          val silhouette: Silhouette,
                          val authorization: SilhouetteAuthorization,
                          override val controllerComponents: ControllerComponents)
  extends AbstractController(controllerComponents) {

  import silhouette._

  def userinfo: Action[AnyContent] = SecuredAction(authorization) { implicit request =>
    val identity: User = request.identity
    val loginInfo =
      LoginInfo(
        firstName = identity.firstName,
        lastName = identity.lastName,
        fullName = identity.fullName,
        email = identity.email,
        avatarURL = identity.avatarURL)
    Ok(Json.toJson(loginInfo))
  }
}

case class LoginInfo(
                      firstName: Option[String],
                      lastName: Option[String],
                      fullName: Option[String],
                      email: Option[String],
                      avatarURL: Option[String])

object LoginInfo {

  implicit val loginInfoFormat: Format[LoginInfo] = Json.format[LoginInfo]
}