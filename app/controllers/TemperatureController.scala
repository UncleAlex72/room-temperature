package controllers

import play.api.libs.json.Json
import play.api.mvc._
import temperature.TemperatureRecordingService

import scala.concurrent.{ExecutionContext, Future}
import temperature.TemperatureReading._

/**
  * The main controller used for interacting with browsers.
  */
class TemperatureController(
                             val maybeTemperatureRecordingService: Option[TemperatureRecordingService],
                             override val controllerComponents: ControllerComponents)(implicit val ec: ExecutionContext)
  extends AbstractController(controllerComponents) {

  def poll = Action.async { implicit request =>
    maybeTemperatureRecordingService match {
      case Some(temperatureRecordingService) =>
        temperatureRecordingService.storeCurrentTemperature().map(tr => Ok(Json.toJson(tr)))
      case None => Future.successful(NotFound(""))
    }
  }
}

