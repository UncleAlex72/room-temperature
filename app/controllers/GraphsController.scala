package controllers

import java.time._

import graph.ReadOnlyGraphsContainerDao
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import uk.co.unclealex.psm._

import scala.concurrent.ExecutionContext

class GraphsController(
                       val clock: Clock,
                       val readOnlyGraphsContainerDao: ReadOnlyGraphsContainerDao,
                       val silhouette: Silhouette,
                       val authorization: SilhouetteAuthorization,
                       override val controllerComponents: ControllerComponents)(implicit executionContext: ExecutionContext)
  extends AbstractController(controllerComponents){

  implicit val timeZone: ZoneId = clock.getZone

  import silhouette._

  def graphs: Action[AnyContent] = SecuredAction(authorization).async { implicit request =>
    readOnlyGraphsContainerDao.getGraphsContainer.map {
      case Some(graphsContainer) => Ok(Json.toJson(graphsContainer))
      case None => NotFound
    }
  }
}

