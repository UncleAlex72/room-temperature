package controllers

import play.api.libs.json.Json
import play.api.mvc._
import temperature.{LegacyLogReaderService, TemperatureDao}
import uk.co.unclealex.psm.{Silhouette, SilhouetteAuthorization}

import scala.concurrent.{ExecutionContext, Future}
import scala.io.{BufferedSource, Source}

/**
  * The main controller used for interacting with browsers.
  */
class ImportController(
            val legacyLogReaderService: LegacyLogReaderService,
            val temperatureDao: TemperatureDao,
            val silhouette: Silhouette,
            val authorization: SilhouetteAuthorization,
            override val controllerComponents: ControllerComponents)(implicit val ec: ExecutionContext)
  extends AbstractController(controllerComponents) {

  import silhouette._

  def importLegacy: Action[RawBuffer] = SecuredAction(authorization).async(parse.raw(2 * 1024 * 1024)) { implicit request =>
    temperatureDao.isEmpty().flatMap { isEmpty =>
      if (isEmpty) {
        val source: BufferedSource = Source.fromFile(request.body.asFile, "UTF-8")
        val lines: Seq[String] = source.getLines().toSeq
        val eventualCount: Future[Int] = legacyLogReaderService.read(lines).foldLeft(Future.successful(0)) { (eventualCount, kv) =>
          val (instant, temperature) = kv
          for {
            count <- eventualCount
            _ <- temperatureDao.insert(temperature, instant)
          } yield {
            count + 1
          }
        }
        eventualCount.map { count =>
          source.close()
          Ok(Json.toJson(count))
        }
      }
      else {
        Future.successful(BadRequest("Database not empty"))
      }
    }
  }
}

