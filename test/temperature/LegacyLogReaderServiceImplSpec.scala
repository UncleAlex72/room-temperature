package temperature

import java.time.{Instant, ZoneId}

import org.scalatest.{Matchers, WordSpec}
import time.HourTimeTruncator

import scala.collection.SortedMap

class LegacyLogReaderServiceImplSpec extends WordSpec with Matchers {

  "Reading a file containing dates and temperatures" should {
    "record and order temperature readings modulus 30 minutes" in {
      val logFile: String =
        """
          |Thu 26 Jul 18:10:59 UTC 2018
          |31.5
          |Thu 26 Jul 18:11:01 UTC 2018
          |31.4
          |Thu 26 Jul 18:11:04 UTC 2018
          |31.4
          |Sat  1 Sep 03:00:01 UTC 2018
          |Sat  1 Sep 03:11:15 UTC 2018
          |22.6
          |Sat  1 Sep 04:00:01 UTC 2018
          |Sat  1 Sep 04:11:15 UTC 2018
          |22.1
        """.stripMargin
      val expectedResults: SortedMap[Instant, BigDecimal] = {
        import uk.co.unclealex.days.Syntax._
        implicit val timeZone: ZoneId = ZoneId.of("UTC")
        implicit val stringToBigDecimal: String => BigDecimal = BigDecimal(_)
        SortedMap(
          (July(26, 2018) at 18._00).toInstant -> "31.5",
          (September(1, 2018) at 3._00).toInstant -> "22.6",
          (September(1, 2018) at 4._00).toInstant -> "22.1",
        )
      }
      new LegacyLogReaderServiceImpl(new HourTimeTruncator()).read(logFile.split("\n")) should ===(expectedResults)
    }
  }
}
