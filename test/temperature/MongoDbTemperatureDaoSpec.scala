package temperature

import java.time.{Clock, Instant}

import reactivemongo.api.DefaultDB
import uk.co.unclealex.mongodb.MongoDbDaoBaseSpec

import scala.concurrent.Future

class MongoDbTemperatureDaoSpec extends MongoDbDaoBaseSpec[TemperatureReading, MongoDbTemperatureDao]("temperature"){

  def nextTime(originalTime: Instant): Instant = originalTime.plusSeconds(10)

  val timeA: Instant = Instant.EPOCH
  val timeB: Instant = nextTime(timeA)
  val timeC: Instant = nextTime(timeB)
  val timeD: Instant = nextTime(timeC)
  val timeE: Instant = nextTime(timeD)

  "Adding a temperature reading" should {
    "store it and return it" in { f =>
      for {
        reading <- f.dao.insert(BigDecimal(20), timeB)
        all <- f.dao.findAll()
      } yield {
        reading._id should !==(None)
        reading.temperature should ===(BigDecimal(20))
        reading.when should ===(timeB)
        all should contain theSameElementsInOrderAs Seq(reading)
      }
    }
  }

  "Checking to see if any temperatures have been recorded" should {
    "correctly identify when no temperatures have been recorded" in { f =>
      f.dao.isEmpty().map { isEmpty =>
        isEmpty should ===(true)
      }
    }
    "correctly identify when some temperatures have been recorded" in { f =>
      for {
        _ <- f.dao.insert(BigDecimal(20), timeA)
        isEmpty <- f.dao.isEmpty()
      } yield {
        isEmpty should ===(false)
      }
    }
  }

  "Searching for temperature readings between dates" should {
    "return all dates when no upper and lower bounds are set" in { implicit f =>
      findBetween(None, None).map { results =>
        results should contain theSameElementsInOrderAs List(
          "10" -> timeA, "20" -> timeB, "30" -> timeC, "40" -> timeD, "50" -> timeE)
      }
    }
    "return no later dates when only an upper bound is set" in { implicit f =>
      findBetween(None, Some(timeC)).map { results =>
        results should contain theSameElementsInOrderAs List(
          "10" -> timeA, "20" -> timeB, "30" -> timeC)
      }
    }
    "return no earlier dates when only a lower bound is set" in { implicit f =>
      findBetween(Some(timeC), None).map { results =>
        results should contain theSameElementsInOrderAs List(
          "30" -> timeC, "40" -> timeD, "50" -> timeE)
      }
    }
    "return no dates outside of either bound when both bounds are set" in { implicit f =>
      findBetween(Some(timeB), Some(timeD)).map { results =>
        results should contain theSameElementsInOrderAs List(
          "20" -> timeB, "30" -> timeC, "40" -> timeD)
      }
    }
  }

  def findBetween(maybeEarliestInstant: Option[Instant], maybeLatestInstant: Option[Instant])
                 (implicit daoAndDatabaseProvider: DaoAndDatabaseProvider): Future[Seq[(String, Instant)]] = {
    val dao: MongoDbTemperatureDao = daoAndDatabaseProvider.dao
    for {
      _ <- dao.insert(BigDecimal(10), timeA)
      _ <- dao.insert(BigDecimal(20), timeB)
      _ <- dao.insert(BigDecimal(30), timeC)
      _ <- dao.insert(BigDecimal(40), timeD)
      _ <- dao.insert(BigDecimal(50), timeE)
      results <- dao.findDuring(maybeEarliestInstant, maybeLatestInstant)
    } yield {
      results.map(reading => reading.temperature.toString() -> reading.when)
    }
  }
  override def createDao(databaseProvider: () => Future[DefaultDB], clock: Clock): MongoDbTemperatureDao = {
    new MongoDbTemperatureDao(databaseProvider, clock)
  }
}
