package temperature

import java.net.InetSocketAddress

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source, Tcp}
import akka.testkit.SocketUtil
import akka.util.ByteString
import com.typesafe.config.ConfigFactory
import org.scalatest.{FutureOutcome, Matchers, fixture}

import scala.concurrent.Future

class TcpThermometerSpec extends fixture.AsyncWordSpec with Matchers {

  override type FixtureParam = TcpThermometer

  "Connecting to a temperature service over TCP" should {
    "successfully read the temperature" in { tcpTemperatureService =>
      for {
        currentTemperature <- tcpTemperatureService.currentTemperature()
      } yield {
        currentTemperature should ===(BigDecimal("50.2"))
      }
    }
  }

  override def withFixture(test: OneArgAsyncTest): FutureOutcome = {

    val localhost: InetSocketAddress = SocketUtil.temporaryServerAddress()

    implicit val actorSystem: ActorSystem = ActorSystem("test", ConfigFactory.empty())
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    val connections: Source[Tcp.IncomingConnection, Future[Tcp.ServerBinding]] =
      Tcp().bind(localhost.getHostString, localhost.getPort)
    connections.runForeach { connection =>
      val serverLogic: Flow[ByteString, ByteString, NotUsed] =
        Flow.fromSinkAndSource(Sink.ignore, Source.single(ByteString("50.2\r\n")))
      connection.handleWith(serverLogic)
    }
    val tcpTemperatureService = new TcpThermometer(localhost.getHostName, localhost.getPort)
    withFixture(test.toNoArgAsyncTest(tcpTemperatureService)).onCompletedThen { _ =>
      actorSystem.terminate()
    }
  }
}