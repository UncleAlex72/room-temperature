package temperature

import java.time.{Clock, Instant, ZoneId, ZonedDateTime}

import org.mockito.Mockito._
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{AsyncWordSpec, Matchers}
import uk.co.unclealex.days.Syntax._

import scala.concurrent.Future

class TemperatureRecordingServiceImplSpec extends AsyncWordSpec with MockitoSugar with Matchers {

  implicit val zoneId: ZoneId = ZoneId.of("Europe/London")

  "The temperature recording service" should {
    "read a temperature and record it" in {
      val thermometer: Thermometer = mock[Thermometer]
      val temperatureDao: TemperatureDao = mock[TemperatureDao]
      val now: Instant = September(5, 1972) at 15._07
      val clock: Clock = Clock.fixed(now, ZoneId.of("Europe/London"))
      val currentTemperature = BigDecimal("30.2")
      val reading: TemperatureReading = TemperatureReading(None, currentTemperature, now)
      when(thermometer.currentTemperature()).thenReturn(Future.successful(currentTemperature))
      when(temperatureDao.insert(currentTemperature, now)).thenReturn(Future.successful(reading))
      val temperatureRecordingServiceImpl: TemperatureRecordingServiceImpl =
        new TemperatureRecordingServiceImpl(
          thermometer = thermometer,
          temperatureDao = temperatureDao,
          timeTruncator = (zonedDateTime: ZonedDateTime) => zonedDateTime,
          clock = clock)
      temperatureRecordingServiceImpl.storeCurrentTemperature().map { result =>
        verify(thermometer).currentTemperature()
        verify(temperatureDao).insert(currentTemperature, now)
        result should ===(reading)

      }
    }
  }
}
