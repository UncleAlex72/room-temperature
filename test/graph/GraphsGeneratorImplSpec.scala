package graph

import java.time._

import uk.co.unclealex.days.Syntax._
import org.scalatest.{Matchers, WordSpec}

class GraphsGeneratorImplSpec extends WordSpec with Matchers {

  implicit val timeZone: ZoneId = ZoneId.of("Europe/London")
  val clock: Clock = Clock.system(timeZone)
  val graphsGeneratorImpl = new GraphsGeneratorImpl(clock)

  def hourToString(hour: Int): String = {
    if (hour == 0) {
      "12am"
    }
    else if (hour == 12) {
      "12pm"
    }
    else if (hour < 12) {
      s"${hour}am"
    }
    else {
      s"${hour - 12}pm"
    }
  }
  "The day and night filters" should {
    Seq(0, 1, 2, 3, 4, 5, 6, 7, 8).foreach { hour =>
        s"treat ${hourToString(hour)} as nighttime" in {
          val timeToTest: Instant = September(5, 1972) at hour.asIs
          graphsGeneratorImpl.toDayGenerator(timeToTest) should ===(None)
          graphsGeneratorImpl.toNightGenerator(timeToTest).get.atZone(timeZone) should ===(September(5, 1972) at 12.am)
        }
    }
    Seq(9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20).foreach { hour =>
      s"treat ${hourToString(hour)} as daytime" in {
        val timeToTest: Instant = September(5, 1972) at hour.asIs
        graphsGeneratorImpl.toDayGenerator(timeToTest).get.atZone(timeZone) should ===(September(5, 1972) at 12.pm)
        graphsGeneratorImpl.toNightGenerator(timeToTest) should ===(None)
      }
    }
    Seq(21, 22, 23).foreach { hour =>
      s"treat ${hourToString(hour)} as the next night" in {
        val timeToTest: Instant = September(5, 1972) at hour.asIs
        graphsGeneratorImpl.toDayGenerator(timeToTest) should ===(None)
        graphsGeneratorImpl.toNightGenerator(timeToTest).get.atZone(timeZone) should ===(September(6, 1972) at 12.am)
      }
    }
  }
  
  "Generating the graphs" should {
    val data: Seq[(Instant, BigDecimal)] = Seq(
      (September(5, 2018) at 12.am) -> "10.0",
      (September(5, 2018) at 1.am) -> "9.0",
      (September(5, 2018) at 2.am) -> "9.0",
      (September(5, 2018) at 3.am) -> "8.0",
      (September(5, 2018) at 4.am) -> "8.0",
      (September(5, 2018) at 5.am) -> "7.0",
      (September(5, 2018) at 6.am) -> "8.0",
      (September(5, 2018) at 7.am) -> "9.0",
      (September(5, 2018) at 8.am) -> "10.0",
      (September(5, 2018) at 9.am) -> "11.0",
      (September(5, 2018) at 10.am) -> "12.0",
      (September(5, 2018) at 11.am) -> "13.0",
      (September(5, 2018) at 12.pm) -> "14.0",
      (September(5, 2018) at 1.pm) -> "15.0",
      (September(5, 2018) at 2.pm) -> "16.0",
      (September(5, 2018) at 3.pm) -> "17.0",
      (September(5, 2018) at 4.pm) -> "18.0",
      (September(5, 2018) at 5.pm) -> "19.0",
      (September(5, 2018) at 6.pm) -> "18.0",
      (September(5, 2018) at 7.pm) -> "17.0",
      (September(5, 2018) at 8.pm) -> "16.0",
      (September(5, 2018) at 9.pm) -> "15.0",
      (September(5, 2018) at 10.pm) -> "14.0",
      (September(5, 2018) at 11.pm) -> "13.0",
      (September(6, 2018) at 12.am) -> "12.0",
      (September(6, 2018) at 1.am) -> "12.0",
      (September(6, 2018) at 2.am) -> "12.0",
      (September(6, 2018) at 3.am) -> "12.0",
      (September(6, 2018) at 4.am) -> "12.0",
      (September(6, 2018) at 5.am) -> "13.0",
      (September(6, 2018) at 6.am) -> "14.0",
      (September(6, 2018) at 7.am) -> "15.0",
      (September(6, 2018) at 8.am) -> "16.0",
      (September(6, 2018) at 9.am) -> "18.0",
      (September(6, 2018) at 10.am) -> "17.0",
      (September(6, 2018) at 11.am) -> "19.0",
      (September(6, 2018) at 12.pm) -> "20.0",
      (September(6, 2018) at 1.pm) -> "21.0",
      (September(6, 2018) at 2.pm) -> "22.0",
      (September(6, 2018) at 3.pm) -> "23.0",
      (September(6, 2018) at 4.pm) -> "24.0",
      (September(6, 2018) at 5.pm) -> "25.0",
      (September(6, 2018) at 6.pm) -> "24.0",
      (September(6, 2018) at 7.pm) -> "23.0",
      (September(6, 2018) at 8.pm) -> "22.0",
      (September(6, 2018) at 9.pm) -> "21.0",
      (September(6, 2018) at 10.pm) -> "20.0",
      (September(6, 2018) at 11.pm) -> "19.0"
    ).map(dataPoint => (dataPoint._1.toInstant, BigDecimal(dataPoint._2)))
    val graphsContainer: GraphsContainer = graphsGeneratorImpl.generateGraphs(data)
    "produce a full graph that does not change the map" in {
      val expectedResults: Seq[Point] = data.map {
        case (instant, temperature) => Point(instant, temperature)
      }
      graphsContainer.full should contain theSameElementsInOrderAs expectedResults
    }
    "produce a daily highs graph" in {
      graphsContainer.dailyHighs should contain theSameElementsInOrderAs Seq(
        Point(September(5, 2018) at 12.pm, BigDecimal("19.0")),
        Point(September(6, 2018) at 12.pm, BigDecimal("25.0"))
      )
    }
    "produce a daily lows graph" in {
      graphsContainer.dailyLows should contain theSameElementsInOrderAs Seq(
        Point(September(5, 2018) at 12.pm, BigDecimal("11.0")),
        Point(September(6, 2018) at 12.pm, BigDecimal("17.0"))
      )
    }
    "produce a nightly highs graph" in {
      graphsContainer.nightlyHighs should contain theSameElementsInOrderAs Seq(
        Point(September(5, 2018) at 12.am, BigDecimal("10.0")),
        Point(September(6, 2018) at 12.am, BigDecimal("16.0")),
        Point(September(7, 2018) at 12.am, BigDecimal("21.0"))
      )
    }
    "produce a nightly lows graph" in {
      graphsContainer.nightlyLows should contain theSameElementsInOrderAs Seq(
        Point(September(5, 2018) at 12.am, BigDecimal("7.0")),
        Point(September(6, 2018) at 12.am, BigDecimal("12.0")),
        Point(September(7, 2018) at 12.am, BigDecimal("19.0"))
      )
    }
  }
}
