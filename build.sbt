import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

name := """room-temperature"""

lazy val root = (project in file(".")).enablePlugins(PlayScala, DockerPlugin, AshScriptPlugin)

scalaVersion := "2.12.6"

resolvers += Resolver.jcenterRepo

val mongoDbVersion = "1.1.7"

libraryDependencies ++= Seq(
  ehcache,
  ws,
  "uk.co.unclealex" %% "mongodb-scala" % mongoDbVersion,
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "com.beachape" %% "enumeratum" % "1.5.13",
  "org.typelevel" %% "cats-core" % "1.0.1",
  "org.reactivemongo" %% "play2-reactivemongo" % "0.16.0-play26",
  "uk.co.unclealex" %% "play-silhouette-mongo" % "1.0.13",
  "com.enragedginger" %% "akka-quartz-scheduler" % "1.6.1-akka-2.5.x"
)

libraryDependencies ++= Seq(
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2",
  "org.mockito" % "mockito-core" % "2.21.0",
  "uk.co.unclealex" %% "mongodb-scala-test" % mongoDbVersion,
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.13",
  "uk.co.unclealex" %% "thank-you-for-the-days" % "1.0.1"
).map(_ % Test)

// Docker

dockerBaseImage := "unclealex72/docker-play-healthcheck:latest"
dockerExposedPorts := Seq(9000)
maintainer := "Alex Jones <alex.jones@unclealex.co.uk>"
dockerRepository := Some("unclealex72")
dockerUpdateLatest := true
javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)
// Releases

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand("stage"), // : ReleaseStep, build server docker image.
  releaseStepCommand("docker:publish"), // : ReleaseStep, build server docker image.
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)
